This module helps to you to track your all nodes operation like Creation, Updation and Deletion.

Installation

1. Download the module and place in your module folder.
2. Enalbe the module.
3. Goto Configuration page [Domain]/admin/config/development/nalsettings
4. For logs -> Goto Admin -> Reports -> Recent Node Action log messages ([Domain]/admin/reports/nalog)