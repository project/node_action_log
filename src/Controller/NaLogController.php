<?php

namespace Drupal\node_action_log\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Returns responses for Nalog routes.
 */
class NaLogController extends ControllerBase {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The tempstore service.
   *
   * @var Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempstore;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('date.formatter'),
      $container->get('form_builder'),
      $container->get('tempstore.private')
    );
  }

  /**
   * Constructs a NaLogController object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   A database connection.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The private tempstore factory variable.
   */
  public function __construct(Connection $database, DateFormatterInterface $date_formatter, FormBuilderInterface $form_builder, PrivateTempStoreFactory $temp_store_factory) {
    $this->database = $database;
    $this->dateFormatter = $date_formatter;
    $this->userStorage = $this->entityTypeManager()->getStorage('user');
    $this->formBuilder = $form_builder;
    $this->tempstore = $temp_store_factory->get('node_action_log');
  }

  /**
   * Displays a listing of Node action logs.
   *
   * @return array
   *   A render array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   */
  public function overview() {
    $classes = static::getLogTypeClassMap();
    $typelabel = static::getTypeLabel();
    $rows = [];
    $filter_query = $this->buildFilterKeyQuery();
    $filter_query_node_type = $this->buildFilterNodeTypeQuery();
    $header = [
      [
        'data' => $this->t('ID'),
        'field' => 'nal.nalid',
        'sort' => 'desc',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      [
        'data' => $this->t('Type'),
        'field' => 'nal.type',
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      [
        'data' => $this->t('Date'),
        'field' => 'nal.timestamp',
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      [
        'data' => $this->t('Node ID'),
        'field' => 'nal.nid',
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      [
        'data' => $this->t('Node Title'),
        'field' => 'nal.title',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      [
        'data' => $this->t('Node Type'),
        'field' => 'nal.bundle',
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      [
        'data' => $this->t('User ID'),
        'field' => 'nal.uid',
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      [
        'data' => $this->t('IP Address'),
        'field' => 'nal.hostname',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
    ];

    $query = $this->database->select('node_action_log', 'nal')
      ->extend('\Drupal\Core\Database\Query\PagerSelectExtender')
      ->extend('\Drupal\Core\Database\Query\TableSortExtender');
    $query->fields('nal', [
      'nalid',
      'nid',
      'title',
      'bundle',
      'uid',
      'timestamp',
      'type',
      'hostname',
    ]);

    $query->limit(50);
    $query->orderByHeader($header);
    if ($filter_query) {
      $query->condition('nal.type', $filter_query, 'IN');
    }
    if ($filter_query_node_type) {
      $query->condition('nal.bundle', $filter_query_node_type, 'IN');
    }
    $result = $query->execute();

    foreach ($result as $nalog) {
      $username = [
        '#theme' => 'username',
        '#account' => $this->userStorage->load($nalog->uid),
      ];
      $path = NULL;
      if ($nalog->type != 'deleted') {
        $path = \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $nalog->nid);
      }
      $nidlink = [
        '#theme' => 'nodelink_template',
        '#title' => $nalog->nid,
        '#link_path' => $path,
      ];
      $titlelink = [
        '#theme' => 'nodelink_template',
        '#title' => $nalog->title,
        '#link_path' => $path,
      ];
      $bundle_label = \Drupal::entityTypeManager()
        ->getStorage('node_type')
        ->load($nalog->bundle)
        ->label();
      $rows[] = [
        'data' => [
          $nalog->nalid,
          new FormattableMarkup('<span> @type </span>', ['@type' => $this->t($typelabel[$nalog->type])]),
          $this->dateFormatter->format($nalog->timestamp, 'medium'),
          ['data' => $nidlink],
          ['data' => $titlelink],
          $this->t($bundle_label),
          ['data' => $username],
          $nalog->hostname,
        ],
        // Attributes for table row.
        'class' => [
          Html::getClass('nalog-' . $nalog->type), $classes[$nalog->type]
        ],
      ];
    }

    $build['nalog_filter_form'] = $this->formBuilder->getForm('Drupal\node_action_log\Form\FilterForm');
    $build['nalog_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => ['id' => 'admin-nalog', 'class' => ['admin-nalog']],
      '#empty' => $this->t('No log messages available.'),
      '#attached' => [
        'library' => ['node_action_log/node_action_log.nalog'],
      ],
    ];
    $build['nalog_pager'] = ['#type' => 'pager'];

    return $build;

  }

  /**
   * Gets an array of log Type classes.
   *
   * @return array
   *   An array of log type classes.
   */
  public static function getLogTypeClassMap() {
    return [
      'created' => 'nalog-created',
      'updated' => 'nalog-updated',
      'deleted' => 'nalog-deleted',
    ];
  }

  /**
   * Gets an array of log Type classes.
   *
   * @return array
   *   An array of log type classes.
   */
  public static function getTypeLabel() {
    return [
      'created' => 'Created',
      'updated' => 'Updated',
      'deleted' => 'Deleted',
    ];
  }

  /**
   * Builds a query for filters based on session.
   *
   * @return array|null
   *   An associative array with values only or NULL if there were
   *   no filters set.
   */
  protected function buildFilterKeyQuery(): ? array {
    if (empty($this->tempstore->get('nal_filter_key'))) {
      return NULL;
    }
    return array_values($this->tempstore->get('nal_filter_key'));
  }

  /**
   * Builds a query for filters based on session.
   *
   * @return array|null
   *   An associative array with values only or void if there were
   *   no filters set.
   */
  protected function buildFilterNodeTypeQuery(): ?array {
    if (empty($this->tempstore->get('nal_filter_node_type'))) {
      return NULL;
    }
    return array_values($this->tempstore->get('nal_filter_node_type'));

  }

}
