<?php

namespace Drupal\node_action_log\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form class.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'node_action_log.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_action_log_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('node_action_log.settings');

    $form['wrapper'] = [
      '#type' => 'details',
      '#title' => $this->t('Configuration'),
      '#open' => TRUE,
    ];

    $days = [];
    for ($i = 1; $i <= 30; $i++) {
      $days[$i] = $i;
    }

    $form['wrapper']['limit'] = [
      '#type' => 'select',
      '#title' => $this->t("Database log messages to keep (Days)"),
      '#default_value' => \Drupal::configFactory()->getEditable('node_action_log.settings')->get('limit'),
      '#options' => $days,
      '#description' => $this->t('Select how long you want to store the logs in database.'),
    ];

    $form['wrapper']['nodeactions'] = [
      '#type' => 'details',
      '#title' => $this->t('Logging Control'),
      '#description' => $this->t('You can control what you want to log (You can\'n disable the delete log, that is module default), Uncheck for disable logging.'),
      '#open' => TRUE,
    ];

    $form['wrapper']['nodeactions']['created'] = [
      '#type' => 'checkbox',
      '#title' => t('Created'),
      '#default_value' => \Drupal::configFactory()->getEditable('node_action_log.settings')->get('created') == 1 ? TRUE : FALSE,
    ];
    $form['wrapper']['nodeactions']['updated'] = [
      '#type' => 'checkbox',
      '#title' => t('Updated'),
      '#default_value' => \Drupal::configFactory()->getEditable('node_action_log.settings')->get('updated') == 1 ? TRUE : FALSE,
    ];

    $form['#attached']['library'][] = 'node_action_log/node_action_log.nalog';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    \Drupal::configFactory()->getEditable('node_action_log.settings')
      ->set('created', $form_state->getValue('created'))
      ->set('updated', $form_state->getValue('updated'))
      ->set('limit', $form_state->getValue('limit'))
      ->save();
  }

}
