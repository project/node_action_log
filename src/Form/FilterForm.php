<?php

namespace Drupal\node_action_log\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the database logging filter form.
 *
 * @internal
 */
class FilterForm extends FormBase {

  /**
   * The tempstore service.
   *
   * @var Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempstore;

  /**
   * Class constructor.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory) {
    $this->tempstore = $temp_store_factory->get('node_action_log');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nal_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $filters = [
      'created' => $this->t('Created'),
      'updated' => $this->t('Updated'),
      'deleted' => $this->t('Deleted'),
    ];
    $form['filters'] = [
      '#type' => 'details',
      '#title' => $this->t('Filter log messages'),
      '#open' => TRUE,
    ];
    $form['filters']['key'] = [
      '#title' => $this->t('Type'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 8,
      '#options' => $filters,
    ];
    if (!is_null($this->tempstore->get('nal_filter_key'))) {
      $form['filters']['key']['#default_value'] = $this->tempstore->get('nal_filter_key');
    }

    $node_types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();

    $filters_node_type = [];
    foreach ($node_types as $node_type) {
      $filters_node_type[$node_type->getOriginalId()] = $this->t($node_type->label());
    }
    uasort($filters_node_type, static function ($a, $b) {
      if ($a == $b) {
        return 0;
      }
      return $a > $b ? -1 : 1;
    });
    $form['filters']['node_type'] = [
      '#title' => $this->t('Node Type'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 8,
      '#options' => $filters_node_type,
    ];
    if (!is_null($this->tempstore->get('nal_filter_node_type'))) {
      $form['filters']['node_type']['#default_value'] = $this->tempstore->get('nal_filter_node_type');
    }

    $form['filters']['actions'] = [
      '#type' => 'actions',
      '#attributes' => ['class' => ['container-inline']],
    ];
    $form['filters']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];
    if (!is_null($this->tempstore->get('nal_filter_key')) || !is_null($this->tempstore->get('nal_filter_node_type'))) {
      $form['filters']['actions']['reset'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#limit_validation_errors' => [],
        '#submit' => ['::resetForm'],
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->isValueEmpty('key') && $form_state->isValueEmpty('node_type')) {
      $form_state->setErrorByName('key', $this->t('You must select atleast any one of the filter option.'));
      $form_state->setErrorByName('node_type', $this->t('You must select atleast one of the filter option.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasValue('key')) {
      $this->tempstore->set('nal_filter_key', $form_state->getValue('key'));
    }
    if ($form_state->hasValue('node_type')) {
      $this->tempstore->set('nal_filter_node_type', $form_state->getValue('node_type'));
    }
  }

  /**
   * Resets the filter form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {
    $this->tempstore->delete('nal_filter_key');
    $this->tempstore->delete('node_type');
  }

}
